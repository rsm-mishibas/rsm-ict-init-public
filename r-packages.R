## load radiant library
library(radiant)

## DO NOT INSTALL R OR PYTHON PACKAGES THAT ARE ALREADY
## PART OF THE RSM-MSBA-SPARK IMAGE/CONTAINER
## CHECK IS YOU DON'T HAVE WHAT YOU NEED AND INSTALL
## ONLY IF NEEDED

## run cluster analysis
describe(shopping)
result <- kclus(shopping, vars = "v1:v6", nr_clus = 3)
summary(result)
plot(result, plots = c("density","bar"))

## store cluster membership
shopping <- store(shopping, result, name = "clus")
head(shopping)

## installing R packages
.libPaths()
install.packages("fortunes")
fortunes::fortune()
installed.packages()["fortunes", "LibPath"]
remove.packages("fortunes")

## installing R packages in a personal library
.libPaths()
Sys.getenv("R_LIBS_USER")
fs::dir_exists(Sys.getenv("R_LIBS_USER"))
install.packages("fortunes", lib = Sys.getenv("R_LIBS_USER"))
installed.packages()["fortunes", "LibPath"]

## trouble shooting
fs::dir_create(Sys.getenv("R_LIBS_USER"), recurse = TRUE)
rstudioapi::restartSession()

## packages will now go to personal directory by default
.libPaths()
remove.packages("fortunes", lib = .libPaths())
install.packages("fortunes")
installed.packages()["fortunes", "LibPath"]

## can you build package that need to be compiled?
## may require you to use Session > Restart R
install.packages("purrr")
packageVersion("purrr")
remove.packages("purrr")

## install the development version of purrr
## remotes::install_github("tidyverse/purrr", lib = Sys.getenv("R_LIBS_USER"))
remotes::install_github("tidyverse/purrr")
packageVersion("purrr")
remove.packages("purrr")

## what packages are installed
installed.packages()[,"Package"]
installed.packages(lib.loc = Sys.getenv("R_LIBS_USER"))[,"Package"]

## cleanup from terminal
# clean
